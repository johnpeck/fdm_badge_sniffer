<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [nRF52 DK Blinky Example](#nrf52-dk-blinky-example)
  - [Dependencies](#dependencies)
    - [GCC ARM toolchain](#gcc-arm-toolchain)
      - [Installing with Ubuntu](#installing-with-ubuntu)
    - [J-Link drivers for programming Nordic development boards](#j-link-drivers-for-programming-nordic-development-boards)
      - [Installing with Ubuntu](#installing-with-ubuntu-1)
  - [Building hex files](#building-hex-files)
  - [Flashing the board](#flashing-the-board)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# nRF52 DK Blinky Example  #

This is code to run on the
[nRF52 DK](https://www.nordicsemi.com/eng/Products/Bluetooth-Smart-Bluetooth-low-energy/nRF52-DK)
board from Nordic

**This repository uses submodules!** Make sure to issue

    git submodule init
	git submodule update

...to bring them in before viewing or editing the design.

## Dependencies ##

### GCC ARM toolchain ###

#### Installing with Ubuntu ####

Add a PPA for the GNU ARM Embedded Toolchain by following the
instructions
[here](https://launchpad.net/~terry.guo/+archive/ubuntu/gcc-arm-embedded).
For reference, I had to:

    sudo add-apt-repository ppa:terry.guo/gcc-arm-embedded
    sudo apt-get update
    sudo apt-get install gcc-arm-none-eabi=4.9.3.2015q3-1trusty1

...where this last command may be different depending on the version
provided by the ppa.


### J-Link drivers for programming Nordic development boards ###

#### Installing with Ubuntu ####

Download the "Software and Documentation Pack" from
[Segger](https://www.segger.com/jlink-software.html).  I downloaded
`jlink_5.12_x86_64.deb`.

Install the software:

    sudo dpkg -i jlink_5.12_x86_64.deb

## Building hex files ##

The `finalize` target will create hex files:

    make finalize

## Flashing the board ##

The `flash` target will flash the board:

    make flash
	
...if the programmer is attached.

