//  Sniffer.c

/*************************************************************************
  SWIPESENSE sniffer
 *************************************************************************/

#include "app_uart.h"
#include "swipesense.h"

#include "app_timer.h"
#include "radio.h"

#include "trace.h"
#include "deviceaddress.h"

/*************************************************************************
  DEFINES used here
 *************************************************************************/

#define sque_len 200

/*************************************************************************
  Definition of state variables
 *************************************************************************/

uint16_t sfront=0, sback=0;
struct {
	uint8_t deviceaddress[deviceaddress_len];
	int8_t rssi;
	uint16_t rev;
} sque[sque_len];


/*************************************************************************
  Definition of functions
 *************************************************************************/

void sniffer_handler (uint8_t packet[], int8_t rssi)
{	
  deviceaddress_copy (&sque[sfront].deviceaddress[0], &packet[2]);
  sque[sfront].rssi = rssi;
  sque[sfront].rev = packet[9] +(packet[10<<8]);
  sfront++;
  if (sfront >= sque_len)
    sfront = 0;
}


void sniffer_process (void)
{
  uint8_t i;
	
  if (sfront != sback)
    {
      sputs("Device address: \"");
      for (i=0;i<deviceaddress_len;i++)
	sput8(sque[sback].deviceaddress[i]);
      sputs("\", RSSI: \"-");
      sputdec (-sque[sback].rssi, 0);
      sputs ("\", Firmware revision: \"");
      sputdec (sque[sback].rev,0);
      sputs("\"");
      sputnl();	
      sback++;
      if (sback>=sque_len)
	sback = 0;
    }
}

void sniffer_init (void)
{
  receive_start (BADGE_FREQ, sniffer_handler);
}
