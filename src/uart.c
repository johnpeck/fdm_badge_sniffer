//   uart.c

/*************************************************************************
  SWIPESENSE uart
 *************************************************************************/

#include "nrf52.h"
#include "nrf52_bitfields.h"
#include "swipesense.h"

#include "nrf_gpio.h"

#include "uart.h"
#include "app_util_platform.h"

/*************************************************************************
  DEFINES used here
 *************************************************************************/
 
#define RX_BUF_LEN 500

/*************************************************************************
  Definition of state variables
 *************************************************************************/

uint8_t rx_buf[RX_BUF_LEN];
uint16_t rx_f, rx_b;
uint8_t uart_initialized=0;
uint16_t last_uart_error = 0;


/*************************************************************************
  Definition of internal functions
 *************************************************************************/

#ifndef serialif
#ifndef TRACESLAVE
void UARTE0_UART0_IRQHandler (void)
{
	
  if (NRF_UART0->EVENTS_ERROR)
    {
      NRF_UART0->EVENTS_ERROR = 0;
      last_uart_error = NRF_UART0->ERRORSRC;
      (NRF_UART0->RXD);
    }
  while (NRF_UART0->EVENTS_RXDRDY)
    {
      uint16_t nbf;
      nbf = rx_f+1;
      if (nbf >= RX_BUF_LEN)
	nbf = 0;
      if (nbf != rx_b)
	{
	  rx_buf[rx_f] = NRF_UART0->RXD;
	  NRF_UART0->EVENTS_RXDRDY = 0;
	  rx_f = nbf;
	}
      else
	{
	  nbf = NRF_UART0->RXD;
	  NRF_UART0->EVENTS_RXDRDY = 0;

	}
    }
}
#endif
#endif


/*************************************************************************
  Definition of external functions
 *************************************************************************/

uint8_t uart_get (uint8_t *ch)
{
	if (uart_initialized)
	{
		if (rx_f == rx_b)
			return (0);
		else
		{
			*ch = rx_buf[rx_b];
			rx_b++;
			if (rx_b >= RX_BUF_LEN)
				rx_b = 0;
			return (1);
		}
	}
	else
		return (0);
}

void uart_put (uint8_t ch)
{

	if (uart_initialized)
	{
		NRF_UART0->TXD = ch;
		while (NRF_UART0->EVENTS_TXDRDY!=1) {}
		NRF_UART0->EVENTS_TXDRDY=0;
	}
}

void uart_deinit(void)
{
	NRF_UART0->ENABLE = 0;
	uart_initialized = 0;
}

void uart_init (void)
{

  rx_f = 0;
  rx_b = 0;

  nrf_gpio_cfg_output(TX_PIN_NUMBER);
  nrf_gpio_cfg_input(RX_PIN_NUMBER, NRF_GPIO_PIN_NOPULL);  
	
  NRF_UART0->ENABLE = 4;
  NRF_UART0->INTENSET = (UART_INTENSET_RXDRDY_Enabled << UART_INTENSET_RXDRDY_Pos) | (UART_INTENSET_ERROR_Enabled << UART_INTENSET_ERROR_Pos);
  NRF_UART0->PSELTXD = TX_PIN_NUMBER;
  NRF_UART0->PSELRXD = RX_PIN_NUMBER;
  NRF_UART0->BAUDRATE = UARTE_BAUDRATE_BAUDRATE_Baud115200;
  NRF_UART0->CONFIG = 0;
  NRF_UART0->TASKS_STARTRX = 1;
  NRF_UART0->TASKS_STARTTX = 1;
  NRF_UART0->EVENTS_RXDRDY = 0;
  NVIC_SetPriority(UARTE0_UART0_IRQn, UART0_IRQ_PRI);  
  NVIC_EnableIRQ(UARTE0_UART0_IRQn);	
  uart_initialized = 1;
}



