#ifndef TRACE_H
#define TRACE_H

//  Trace.h

/*************************************************************************
   trace utility module
 *************************************************************************/


/*************************************************************************
  Definition of functions
 *************************************************************************/


//  Trace commands

void sputs(const char *str);
void sputnl (void);

void sput32 (uint32_t val);
void sput16 (uint16_t val);
void sput8 (uint8_t val);
void sputdec (uint32_t val, uint32_t decimal);

void spacket (uint8_t packet[]);
void sdevice (uint8_t da[]);

void trace_process(void);
void trace_process2(void);
void trace_put (uint8_t ch);
void trace_init (uint8_t uartinit);
void trace_deinit (void);


#endif
