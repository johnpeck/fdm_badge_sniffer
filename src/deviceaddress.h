#ifndef DEVICEADDRESS_H
#define DEVICEADDRESS_H

// Deviceaddress.h

/*************************************************************************
  Deviceaddress handling module (UTC) module
 *************************************************************************/

/*************************************************************************
  DEFINES used here
 *************************************************************************/


#define deviceaddress_len 6


/*************************************************************************
  Definition of functions
 *************************************************************************/


void return_deviceaddress (uint8_t *deviceaddress);

uint8_t deviceaddress_matches (uint8_t *da1, uint8_t *da2);

void	deviceaddress_copy (uint8_t *dest, uint8_t *source);

#endif
