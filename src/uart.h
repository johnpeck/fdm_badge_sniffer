#ifndef UART_H
#define UART_H

//  Uart.h  Standard uart device driver used by trace


/*************************************************************************
   standard uart module
 *************************************************************************/


/*************************************************************************
  Definition of functions
 *************************************************************************/

void uart_deinit(void);
		
void uart_init (void);

void uart_put (uint8_t ch);

uint8_t uart_get (uint8_t *ch);

#endif
