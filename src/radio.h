#ifndef RADIO_H
#define RADIO_H

/*************************************************************************
  SWIPESENSE Radio module
 *************************************************************************/


/*************************************************************************
  DEFINES used here
 *************************************************************************/

#define PACKET_BASE_ADDRESS_LENGTH  (4UL)                   //!< Packet base address length field size in bytes
#define PACKET_STATIC_LENGTH        (1UL)                   //!< Packet static length in bytes
#define PACKET_PAYLOAD_MAX      (255)  //!< Packet payload maximum size in bytes

#define RADIO_PACKET_LEN		256

/*************************************************************************
  TYPES used here
 *************************************************************************/

typedef void (*radio_handler_t)(uint8_t packet[], int8_t rssi);

/*************************************************************************
  Definition of functions
 *************************************************************************/


void radio_configure(void);

void receive_start (uint8_t freq, radio_handler_t rec_handler);

void receive_stop (void);

void radio_wait_send_idle (void);

uint8_t radio_devmatch (void);
uint8_t radio_devmiss (void);

void send_packet (uint8_t freq, uint8_t *packet);




#endif
