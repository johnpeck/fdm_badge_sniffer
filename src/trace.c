/* Copyright (c) 2016 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */
#include "nrf52.h"
#include "nrf52_bitfields.h"
#include "app_error.h"
#include "trace.h"
#include "swipesense.h"
#include "nrf_delay.h"
#include "nrf_gpio.h"

#include "uart.h"

#include "app_util_platform.h"
#include "deviceaddress.h"

/*************************************************************************
  DEFINES used here
 *************************************************************************/


#define TB_MAX 4000
#define TRACE_BUFF_NUM 400
#define TRACE_BUFF_COUNT 4

/*************************************************************************
  State vriables
 *************************************************************************/


uint8_t trace_buf[TB_MAX];
uint16_t tbf, tbb;
uint16_t fails, sbuf_f, sbuf_b, curpos, tracespi_done;

#ifdef TRACESLAVE
uint32_t align9;
uint8_t dummy_buffer[TRACE_BUFF_NUM];
uint8_t rx_spis_buffer[TRACE_BUFF_COUNT][TRACE_BUFF_NUM];
#endif

/*************************************************************************
  Definition of functions
 *************************************************************************/


#ifdef TRACESLAVE
void 	traces_fill_buffer_set (void)
{
uint16_t i;
	
	for (i=0;i<TRACE_BUFF_NUM;i++)
	{
		rx_spis_buffer[sbuf_f][i]=0xff;		
	}
	NRF_SPIS0->TASKS_ACQUIRE = 1;
//	while (NRF_SPIS0->EVENTS_ACQUIRED==0) {}

	NRF_SPIS0->RXD.PTR = (uint32_t) &rx_spis_buffer[sbuf_f++][0];
//	NRF_SPIS0->TASKS_RELEASE = 1;
		
	if (sbuf_f>=TRACE_BUFF_COUNT)
		sbuf_f = 0;
}
#endif



uint8_t trace_get (void)
{
	uint8_t retval = trace_buf[tbb];
		tbb++;
		if (tbb>=TB_MAX)
			tbb=0;
		return (retval);
}

#ifdef tracespi

void SPIM0_SPIS0_TWIM0_TWIS0_SPI0_TWI0_IRQHandler (void)
{

#ifdef TRACESLAVE

		if (NRF_SPIS0->EVENTS_ACQUIRED)
		{
			traces_fill_buffer_set();
			NRF_SPIS0->TASKS_RELEASE=1;
			NRF_SPIS0->EVENTS_ACQUIRED = 0;
			(NRF_SPIS0->EVENTS_ACQUIRED);
		}

		if (NRF_SPIS0->EVENTS_END)
			NRF_SPIS0->TASKS_ACQUIRE = 1;
	
#else
	
	if (NRF_SPI0->EVENTS_READY)
	{
		tracespi_done = 1;
		NRF_SPI0->EVENTS_READY = 0;
		(NRF_SPI0->EVENTS_READY);
	}
#endif		

}

#endif


/*************************************************************************
  Definition of external functions
 *************************************************************************/

void trace_put (uint8_t ch)
{
	uint16_t nbf;
	nbf = tbf+1;
	if (nbf>=TB_MAX)
		nbf=0;
	if (nbf!=tbb)
	{
		trace_buf[tbf] = ch;
		tbf = nbf;
	}
	else
		fails++;
}

void putdig (uint16_t dig)
{
		if (dig < 10)
			trace_put (0x30+dig);
		else
			trace_put (0x41+dig-10);
}

void puthex (uint16_t val)
{
		putdig (val>>4);
		putdig (val&0xf);
}

void sput32 (uint32_t val)
{
	trace_put (0x20);
	puthex (val>>24);
	puthex ((val>>16)&0xff);
	puthex ((val>>8)&0xff);
	puthex (val&0xff);
}

void sput16 (uint16_t val)
{
	trace_put (0x20);
	puthex ((val>>8)&0xff);
	puthex (val&0xff);
}

void sput8 (uint8_t val)
{
	trace_put (0x20);
	puthex (val&0xff);
}

void sputdec (uint32_t val, uint32_t decimal)
{
uint32_t sum, divisor, dig;
uint8_t zs;
	zs = 1;
	divisor = 1000000000;
	sum = val;
	while (divisor >0)
	{

		dig = sum/divisor;
		sum = sum - dig*divisor;

		if (dig==0) 
		{
			if ((zs==0) || (divisor==1) ) 
				putdig (dig);
		}
		else
		{
			putdig(dig);
			zs=0;
		}
		divisor = divisor/10;
		if ((decimal==divisor) && (decimal))
		{
			if (zs)
			{
				zs=0;
				sputs ("0");
			}
			sputs (".");
		}
	}
	sputs(" ");
}

void sputs (const char *str)
{
  uint_fast8_t i = 0;
  uint8_t ch = str[i++];
  while (ch != '\0')
  {
    trace_put(ch);
    ch = str[i++];
  }
}

void sputnl (void)
{
	sputs ("\r\n");
}

void spacket (uint8_t packet[])
{
uint8_t i;
	for (i=0;i<=packet[0];i++)
		sput8(packet[i]);
	sputnl();
}

void sdevice (uint8_t da[])
{
uint8_t i;
	for (i=0;i<deviceaddress_len;i++)
		sput8(da[i]);	
}

/*lint -save -e14 */

/**
 * Function is implemented as weak so that it can be overwritten by custom application error handler
 * when needed.
 */
__WEAK void app_error_fault_handler(uint32_t id, uint32_t pc, uint32_t info)
{
    // On assert, the system can only recover with a reset.

	sputs ("app_error_fault_handler  ");
	sput32 (id);
	sput32 (pc);
	sput32 (info);
	sputnl();
	
	while (1) {};
}

void trace_process(void)
{
#ifdef TRACESLAVE

		while (sbuf_b != sbuf_f) 
		{
			if (rx_spis_buffer[sbuf_b][curpos]==0xff) 
				break;
			uart_put(rx_spis_buffer[sbuf_b][curpos]);
			curpos++;
			if (curpos >= TRACE_BUFF_NUM)
			{
				sbuf_b++;
				if (sbuf_b>=TRACE_BUFF_COUNT)
					sbuf_b=0;
				curpos = 0;
			}
		}

#else
#ifdef tracespi	
	if (tracespi_done)
	{
			nrf_gpio_pin_set (TRACE_SEL_PIN_NUMBER);
			nrf_delay_us (10);
	}

	if (tbf!=tbb)
	{		
			nrf_gpio_pin_clear (TRACE_SEL_PIN_NUMBER);
			nrf_delay_us (10);
			NRF_SPI0->TXD = trace_get();
	}
#else
	while (tbf!=tbb)
		uart_put(trace_get());
#endif
#endif
}


void trace_deinit (void)
{
		uart_deinit();
}

void trace_init (uint8_t uartinit)
{
	tbf = 0;
	tbb = 0;	
#ifdef tracespi

#ifndef TRACESLAVE
	nrf_gpio_cfg_output (TRACE_SEL_PIN_NUMBER);
	nrf_gpio_pin_set (TRACE_SEL_PIN_NUMBER);	
	nrf_gpio_cfg_output (TRACE_CLK_PIN_NUMBER);
	nrf_gpio_pin_set (TRACE_CLK_PIN_NUMBER);	
	nrf_gpio_cfg_output (TRACE_DATA_PIN_NUMBER);
	nrf_gpio_pin_set (TRACE_DATA_PIN_NUMBER);	
	NRF_SPI0->CONFIG = 0;
	NRF_SPI0->INTENSET = 4;
	NRF_SPI0->PSEL.SCK = TRACE_CLK_PIN_NUMBER;
	NRF_SPI0->PSEL.MOSI = TRACE_DATA_PIN_NUMBER;
	NRF_SPI0->PSEL.MISO = 26;
	NRF_SPI0->FREQUENCY = 0x10000000;
	NRF_SPI0->ENABLE = 1;
	tracespi_done=1;
	NVIC_SetPriority(SPIM0_SPIS0_TWIM0_TWIS0_SPI0_TWI0_IRQn, UART_TIMER_IRQ_PRI); 
 	NVIC_EnableIRQ(SPIM0_SPIS0_TWIM0_TWIS0_SPI0_TWI0_IRQn);
#else
	nrf_gpio_cfg_sense_input (TRACE_SEL_PIN_NUMBER, NRF_GPIO_PIN_NOPULL,NRF_GPIO_PIN_SENSE_HIGH);
	NRF_GPIO->PIN_CNF[TRACE_SEL_PIN_NUMBER] = 0x20000;	
	nrf_gpio_cfg_sense_input (TRACE_CLK_PIN_NUMBER, NRF_GPIO_PIN_NOPULL,NRF_GPIO_PIN_SENSE_HIGH);
	NRF_GPIO->PIN_CNF[TRACE_CLK_PIN_NUMBER] = 0x20000;
	nrf_gpio_cfg_sense_input (TRACE_DATA_PIN_NUMBER, NRF_GPIO_PIN_NOPULL,NRF_GPIO_PIN_SENSE_HIGH);
	NRF_GPIO->PIN_CNF[TRACE_DATA_PIN_NUMBER] = 0x20000;

	
	sbuf_f = 0;
	sbuf_b = 0;
	curpos = 0;
	NRF_SPIS0->SHORTS = 4;
	NRF_SPI0->CONFIG = 0;
	NRF_SPIS0->PSEL.SCK = TRACE_CLK_PIN_NUMBER;
	NRF_SPIS0->PSEL.CSN = TRACE_SEL_PIN_NUMBER;
	NRF_SPIS0->PSEL.MISO = 0xffff;
	NRF_SPIS0->PSEL.MOSI = TRACE_DATA_PIN_NUMBER;
	NRF_SPIS0->TXD.PTR = (uint32_t) &dummy_buffer[0];
	NRF_SPIS0->TXD.MAXCNT = TRACE_BUFF_NUM;
	NRF_SPIS0->RXD.MAXCNT = TRACE_BUFF_NUM;
	NRF_SPIS0->INTENSET = 0x402;
	NVIC_SetPriority(SPIM0_SPIS0_TWIM0_TWIS0_SPI0_TWI0_IRQn, UART_TIMER_IRQ_PRI); 
 	NVIC_EnableIRQ(SPIM0_SPIS0_TWIM0_TWIS0_SPI0_TWI0_IRQn);	

	NRF_SPIS0->ENABLE = 1;
	NRF_SPIS0->TASKS_ACQUIRE = 1;
	uart_init();
#endif
	
#else	
	if (uartinit)
		uart_init();
#endif
}


/*lint -restore */



