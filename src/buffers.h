#ifndef BUFFERS_H
#define BUFFERS_H

#define MAX_PACKET_SIZE 0xff

typedef struct {
	uint16_t front;
	uint16_t back;
} buffer_queue_t;

void buffer_queue_init (buffer_queue_t *queue);

uint8_t buffer_queue (buffer_queue_t *queue, uint8_t packet[]);

uint8_t buffer_remove (uint8_t packet[], buffer_queue_t *queue);

#ifdef TRACE
uint16_t buffer_free_count (void);
uint16_t buffer_qtrace (buffer_queue_t *queue);
#endif

void buffers_init(void);

#endif
