/* Copyright (c) 2014 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */
 
//  Radio.c

#include "nrf52.h"
#include "nrf52_bitfields.h"
#include "radio.h"
#include "swipesense.h"
#include "nrf_delay.h"
#include "trace.h"
#include "app_util_platform.h"


/*************************************************************************
  DEFINES used here
*************************************************************************/


#define PACKET_S1_FIELD_SIZE      (0UL)  /**< Packet S1 field size in bits. */
#define PACKET_S0_FIELD_SIZE      (0UL)  /**< Packet S0 field size in bits. */
#define PACKET_LENGTH_FIELD_SIZE  (8UL)  /**< Packet length field size in bits. */

#define RadioStateNull 0
#define RadioStateSendEnd 1
#define RadioStateReceiveEnd 2


/*************************************************************************
  Definition of state variables
*************************************************************************/

int16_t RadioState = RadioStateNull;
int16_t RadioStateDevMatch=0, RadioStateDevMiss=0;

radio_handler_t receive_handler;

uint8_t rx_buffer[RADIO_PACKET_LEN];

/*************************************************************************
  Definition of functions
*************************************************************************/

void radio_configure()
{
#ifdef TRACE
  sputs ("radio_configure\r\n");
#endif
	
  NRF_RADIO->POWER = 0;
  nrf_delay_us (100000);
  NRF_RADIO->POWER = 1;
  nrf_delay_us (100000);
	
  NRF_RADIO->MODECNF0 = 1;
	
  // Radio config
  NRF_RADIO->TXPOWER   = (TX_POWER << RADIO_TXPOWER_TXPOWER_Pos);
  NRF_RADIO->FREQUENCY = MESH_FREQ;  
  NRF_RADIO->MODE      = (RADIO_MODE_MODE_Nrf_1Mbit << RADIO_MODE_MODE_Pos);
  NRF_RADIO->MODECNF0 = (RADIO_MODECNF0_RU_Fast<< RADIO_MODECNF0_RU_Pos); 
	
  // Radio address config
  NRF_RADIO->PREFIX0 = 
    ((uint32_t)(0xC3) << 24) 
    | ((uint32_t)(0xC2) << 16) 
    | ((uint32_t)(0xC1) << 8)  
    | ((uint32_t)(0xC0) << 0);
  
  NRF_RADIO->PREFIX1 = 
    ((uint32_t)(0x6C) << 24) 
    | ((uint32_t)(0x66) << 16) 
    | ((uint32_t)(0x6C) << 8) 
    | ((uint32_t)(0x62) << 0);

  NRF_RADIO->BASE0 = (0x01234567UL);  // Base address for prefix 0 converted to nRF24L series format
  NRF_RADIO->BASE1 = (0x89ABCDEFUL);  // Base address for prefix 1-7 converted to nRF24L series format
  
  NRF_RADIO->TXADDRESS   = 0x00UL;  // Set device address 0 to use when transmitting
  NRF_RADIO->RXADDRESSES = 0x01UL;  // Enable device address 0 to use to select which addresses to receive

  // Packet configuration
  NRF_RADIO->PCNF0 = (PACKET_S1_FIELD_SIZE     << RADIO_PCNF0_S1LEN_Pos) |
    (PACKET_S0_FIELD_SIZE     << RADIO_PCNF0_S0LEN_Pos) |
    (PACKET_LENGTH_FIELD_SIZE << RADIO_PCNF0_LFLEN_Pos); //lint !e845 "The right argument to operator '|' is certain to be 0"

  // Packet configuration
  NRF_RADIO->PCNF1 = (RADIO_PCNF1_WHITEEN_Enabled << RADIO_PCNF1_WHITEEN_Pos) |
    (RADIO_PCNF1_ENDIAN_Big       << RADIO_PCNF1_ENDIAN_Pos)  |
    (PACKET_BASE_ADDRESS_LENGTH   << RADIO_PCNF1_BALEN_Pos)   |
    (PACKET_STATIC_LENGTH         << RADIO_PCNF1_STATLEN_Pos) |
    (PACKET_PAYLOAD_MAX       << RADIO_PCNF1_MAXLEN_Pos); //lint !e845 "The right argument to operator '|' is certain to be 0"

  // CRC Config
  NRF_RADIO->CRCCNF = (RADIO_CRCCNF_LEN_Two << RADIO_CRCCNF_LEN_Pos); // Number of checksum bits
  if ((NRF_RADIO->CRCCNF & RADIO_CRCCNF_LEN_Msk) == (RADIO_CRCCNF_LEN_Two << RADIO_CRCCNF_LEN_Pos))
    {
      NRF_RADIO->CRCINIT = 0xFFFFUL;   // Initial value      
      NRF_RADIO->CRCPOLY = 0x11021UL;  // CRC poly: x^16+x^12^x^5+1
    }
  else if ((NRF_RADIO->CRCCNF & RADIO_CRCCNF_LEN_Msk) == (RADIO_CRCCNF_LEN_One << RADIO_CRCCNF_LEN_Pos))
    {
      NRF_RADIO->CRCINIT = 0xFFUL;   // Initial value
      NRF_RADIO->CRCPOLY = 0x107UL;  // CRC poly: x^8+x^2^x^1+1
    }
		
  // Set correct IRQ priority and clear any possible pending interrupt.
  NVIC_SetPriority(RADIO_IRQn, RADIO_IRQ_PRI);    
  NVIC_ClearPendingIRQ(RADIO_IRQn);
    
  // Enable IRQ.    

  NRF_RADIO->INTENSET = (RADIO_INTENSET_END_Enabled << RADIO_INTENSET_END_Pos);//???? | (RADIO_INTENSET_DEVMATCH_Enabled << RADIO_INTENSET_DEVMATCH_Pos)
  //		| (RADIO_INTENSET_DEVMISS_Enabled << RADIO_INTENSET_DEVMISS_Pos);
  NRF_RADIO->SHORTS = RADIO_SHORTS_READY_START_Enabled << RADIO_SHORTS_READY_START_Pos
    | (RADIO_SHORTS_ADDRESS_RSSISTART_Enabled<<RADIO_SHORTS_ADDRESS_RSSISTART_Pos)
    | (RADIO_SHORTS_DISABLED_RSSISTOP_Enabled<<RADIO_SHORTS_DISABLED_RSSISTOP_Pos);

  RadioState = RadioStateNull;

  NRF_RADIO->TASKS_DISABLE   = 1U; // Disable the radio.
  NVIC_EnableIRQ(RADIO_IRQn);		
}


/**@brief Function for sending packet.
 */

void send_packet (uint8_t freq, uint8_t *packet)
{
  NRF_RADIO->TASKS_DISABLE = 1U;
  while (NRF_RADIO->EVENTS_DISABLED == 0U) {}
			
  // Set payload pointer.
	
  NRF_RADIO->PACKETPTR = (uint32_t) packet;
  NRF_RADIO->FREQUENCY = freq;
	
  // Start pre-transmit work

  NRF_RADIO->EVENTS_READY = 0U;
  NRF_RADIO->EVENTS_END = 0U;
  RadioState = RadioStateSendEnd;
  NRF_RADIO->TASKS_TXEN   = 1; // Enable radio and wait for ready.
}

void SendPacketEnd (void)
{
  RadioState = RadioStateNull;
  NRF_RADIO->EVENTS_END  = 0U;
  NRF_RADIO->TASKS_DISABLE   = 1U; // Disable the radio.
  while (NRF_RADIO->EVENTS_DISABLED == 0U) {}
}

// receive

void receive_start (uint8_t freq, radio_handler_t rec_handler)
{
  NRF_RADIO->TASKS_DISABLE = 1U;
  NRF_RADIO->PACKETPTR    = (uint32_t)&rx_buffer[0];
  NRF_RADIO->FREQUENCY = freq;
  NRF_RADIO->EVENTS_READY = 0U;
  NRF_RADIO->EVENTS_END = 0U;			

  receive_handler = rec_handler;

  // Wait for the end of the packet.

  RadioState = RadioStateReceiveEnd;
  RadioStateDevMatch = 0;
  RadioStateDevMiss = 0;
  NRF_RADIO->TASKS_RXEN   = 1U; // Enable radio.
}

void ReceivePacketEnd (void)
{
  int16_t rssi;
  rssi = NRF_RADIO->RSSISAMPLE;
  NRF_RADIO->EVENTS_END = 0;

  if (NRF_RADIO->CRCSTATUS)
    {
      receive_handler (&rx_buffer[0], -(rssi));
    }
  NRF_RADIO->TASKS_START = 1;
}

void receive_stop (void)
{
  RadioState = RadioStateNull;
  NRF_RADIO->EVENTS_END = 0;
  NRF_RADIO->TASKS_DISABLE   = 1U; // Disable the radio.	
  while (NRF_RADIO->EVENTS_DISABLED == 0U) {}
}

void radio_wait_send_idle (void)
{
  while (1)
    {
      nrf_delay_us (1);
      if (RadioState==RadioStateNull)
	break;
      if (NRF_RADIO->EVENTS_END)
	{
	  SendPacketEnd();
	  break;
	}
			
    }
}

uint8_t radio_devmatch (void)
{
  return (RadioStateDevMatch);
}

void RADIO_IRQHandler (void)
{

  switch (RadioState)
    {
    case RadioStateSendEnd:
      if (NRF_RADIO->EVENTS_END)
	SendPacketEnd();
      break;
    case RadioStateReceiveEnd:
      if (NRF_RADIO->EVENTS_DEVMATCH)
	{
	  RadioStateDevMatch = 1;
	  NRF_RADIO->EVENTS_DEVMATCH = 0;
	}
      else if (NRF_RADIO->EVENTS_DEVMISS)
	{
	  RadioStateDevMiss = 1;
	  NRF_RADIO->EVENTS_DEVMISS = 0;
	}
      if (NRF_RADIO->EVENTS_END)
	ReceivePacketEnd();
      break;
    default:
      break;
    }
}
