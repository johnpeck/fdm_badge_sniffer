#ifndef UNIXTIME_H
#define UNIXTIME_H

void unixtime_init (void);

#define deviceaddress_len 6

void unixtime_set (uint32_t utc);

uint32_t unixtime_return (void);

void return_deviceaddress (uint8_t *deviceaddress);



#endif
