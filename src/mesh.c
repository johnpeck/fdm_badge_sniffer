//  Mesh.c


/*************************************************************************
  SWIPESENSE Mesh.c
 *************************************************************************/

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "radio_config.h"
#include "nrf_gpio.h"
#include "app_timer.h"
#include "swipesense.h"

#include "trace.h"

#include "sniffer.h"

#include "radio.h"
#include "mesh.h"
#include "nrf_delay.h"

#include "nordic_common.h"
#include "nrf_error.h"
#include "unixtime.h"
#include "buffers.h"



#include "app_util_platform.h"
#include "deviceaddress.h"


/*************************************************************************
  DEFINES used here
 *************************************************************************/

#define MESH_CYCLE_MS ((uint32_t)500)
#define MAX_HUBS 20
#define MESH_TIMEOUT_US 600
#define MESH_TIMEOUT_US_SLAVE 1500
#define MESH_SUPERVISION_TIMEOUT_MAX 8
#define hub_null 0
#define hub_searching 1
#define hub_idle 2
#define hub_connected 3

#define MIN_HUB_RSSI -74

#define timer_stop_value (((uint32_t)16000)*MESH_CYCLE_MS)
#define early_timer_stop_value (timer_stop_value-10)
#define late_timer_stop_value (timer_stop_value+10)

#define rcv_window (((uint32_t)16)*700)
#define early_rcv_window (rcv_window-200)
#define late_rcv_window (rcv_window+200)
#define SendDelayAfterSlotMaster 100
#define SendDelayAfterSlot 45
#define DelayAfterFind (MESH_CYCLE_MS*1047)
//#define DelayAfterFind (MESH_CYCLE_MS*10310/10)///10483

#define BEST_COUNT_MAX 10

#define nullhubindex 0xff

#define null_seq 0x100

/*************************************************************************
  Definition of state variables
 *************************************************************************/

uint32_t cycles=0;
uint8_t hub_state, in_sync;

uint16_t currenthub = 0;
uint16_t hubcount = 0;
struct {
	uint8_t deviceaddress [deviceaddress_len];
	uint8_t settime;
	uint8_t connected;
	uint8_t supervision_timeout_count;
	uint16_t dl_seqr, dl_seqs;
	uint8_t tx_ackpending, rx_ackpending;
}	hubs[MAX_HUBS];

uint8_t tx_packet [PACKET_PAYLOAD_MAX], lasttx_packet[PACKET_PAYLOAD_MAX], rx_packet[PACKET_PAYLOAD_MAX];
uint8_t mesh_receive_flag = 0, mesh_receive_page = 0;
uint16_t supervision_timeouts = 0, supervision_hits = 0, ul_seqs=0, ul_seqr=0;
uint8_t best_rssi_addr[deviceaddress_len];
uint8_t best_rssi_type;
int8_t best_rssi;
buffer_queue_t uplinkqueue;

uint8_t best_count;
uint8_t flash_erase_requested=0, flash_write_requested=0;
uint32_t *flash_page_requested;
uint32_t *flash_image_requested;

/*************************************************************************
  Definition of internal functions
 *************************************************************************/

uint8_t empty_packet (uint8_t packet[])
{
uint16_t i;
	
	for (i=0;i<deviceaddress_len;i++)
		if (packet[MESH_PACKET_DESTID+i] != 0)
			return 0;
	return 1;
}

uint8_t our_packet (uint8_t packet[])
{
uint16_t i;
	
	for (i=0;i<deviceaddress_len;i++)
		if (packet[MESH_PACKET_DESTID+i] != tx_packet[MESH_PACKET_SELFID+i])
			return 0;
	return 1;
}

void zerodest (void)
{
uint16_t i;
	
	for (i=0;i<deviceaddress_len;i++)
		tx_packet[MESH_PACKET_DESTID+i] = 0;
}

void copydest (uint8_t ind)
{
	deviceaddress_copy (&tx_packet [MESH_PACKET_DESTID], &hubs [ind].deviceaddress[0]);
}

void copylast (void)
{
uint8_t i;

	for (i=0;i<tx_packet[0]+1;i++)
		lasttx_packet[i] = tx_packet[i];
}

void setpackettype (uint8_t packet_type, uint8_t len)
{
	tx_packet[MESH_PACKET_TYPE] = packet_type;
	tx_packet[MESH_PACKET_LENGTH] = len;
}

void setcmd (uint8_t cmd_type, uint8_t len)
{
	tx_packet[MESH_PACKET_TYPE] = MESH_PACKET_TYPE_PAYLOAD;
	tx_packet[MESH_PACKET_CMD] = cmd_type;
	tx_packet[MESH_PACKET_LENGTH] = len;
}

void mesh_timer_start (void)
{
	NRF_TIMER0->MODE        = TIMER_MODE_MODE_Timer;
	NRF_TIMER0->BITMODE     = TIMER_BITMODE_BITMODE_24Bit << TIMER_BITMODE_BITMODE_Pos;
	NRF_TIMER0->PRESCALER   = 0;
	NRF_TIMER0->SHORTS			= 1<<TIMER_SHORTS_COMPARE0_CLEAR_Pos;

	// Clears the timer, sets it to 0.
	NRF_TIMER0->TASKS_CLEAR = 1;

	// Load the initial values to TIMER0 CC registers.
	NRF_TIMER0->CC[0] = 16000*MESH_CYCLE_MS;

	// Start the timer.
	NRF_TIMER0->INTENSET = TIMER_INTENSET_COMPARE0_Enabled<<TIMER_INTENSET_COMPARE0_Pos;
	NRF_TIMER0->EVENTS_COMPARE[0] = 0;
	NRF_TIMER0->TASKS_START = 1;
	NVIC_SetPriority (TIMER0_IRQn, TIMER0_IRQ_PRI);
	NVIC_EnableIRQ(TIMER0_IRQn);
}

void mesh_timer_enable(void)
{
	NRF_TIMER0->TASKS_CLEAR = 1;	
	NRF_TIMER0->EVENTS_COMPARE[0] = 0;
	NRF_TIMER0->TASKS_START = 1;
}

void mesh_timer_disable (void)
{
	NRF_TIMER0->TASKS_STOP = 1;
}

void mesh_timeout_start (uint32_t timeout_us)
{
    NRF_TIMER1->MODE        = TIMER_MODE_MODE_Timer;
    NRF_TIMER1->BITMODE     = TIMER_BITMODE_BITMODE_24Bit << TIMER_BITMODE_BITMODE_Pos;
    NRF_TIMER1->PRESCALER   = 0;
		NRF_TIMER1->SHORTS			= 1<<TIMER_SHORTS_COMPARE0_CLEAR_Pos;

    // Clears the timer, sets it to 0.
    NRF_TIMER1->TASKS_CLEAR = 1;

    // Load the initial values to TIMER2 CC registers.
    NRF_TIMER1->CC[0] = 16*timeout_us;

		NRF_TIMER1->CC[2] = early_rcv_window;
		NRF_TIMER1->CC[3] = late_rcv_window;
	
    // Start the timer.
    NRF_TIMER1->TASKS_START = 1;
		NRF_TIMER1->EVENTS_COMPARE[0] = 0;
}

void mesh_timeout_stop ()
{
    // Stop the timer.
    NRF_TIMER1->TASKS_STOP = 1;
}


uint8_t mesh_timeout (void)
{
		return (NRF_TIMER1->EVENTS_COMPARE[0]);
}

//  Handles packet from superior hub

void handle_dl_rx_packet (uint8_t packet[], int8_t rssi)
{
	
uint32_t utc;

#ifdef TRACE
	sputs ("&");
#endif
	
	//  Is this ours
	
	if (hub_state == hub_idle)
	{
		if ( (packet[MESH_PACKET_TYPE] == MESH_PACKET_TYPE_BROADCAST_COMMHUB_OPEN)||(packet[MESH_PACKET_TYPE] == MESH_PACKET_TYPE_BROADCAST_ROOMHUB_OPEN) )
		{
		}
		else if (our_packet (packet))
		{
			hub_state = hub_connected;
		}
		else
			return;
	}
	else
	{
		if (our_packet (packet)==0)
			return;
		hub_state = hub_connected;
	}
	mesh_receive_flag = 1;
	supervision_timeouts = 0;
	
#ifdef TRACE
	sputs ("handle_dl_rx_packet"); sput8(packet[0]); sputnl();
#endif
	
	if (ul_seqr == packet[MESH_PACKET_SEQS])
	{
//  Duplicate, so resend		
#ifdef TRACE
		sputs ("Dup seq"); sput8(ul_seqr); sput8(packet[MESH_PACKET_SEQS]); sputnl();
#endif
		nrf_delay_us (SendDelayAfterSlot);
		send_packet (MESH_FREQ, &lasttx_packet[0]);
		radio_wait_send_idle();
	}
	else
	{
//  Handle receive		
		if (packet[MESH_PACKET_LENGTH])
		{
			if ( (packet[MESH_PACKET_TYPE]==MESH_PACKET_TYPE_PAYLOAD) && (packet[MESH_PACKET_LENGTH]>=MESH_PACKET_CMD) )
			{
				switch (packet[MESH_PACKET_CMD])
				{
					case MESH_CMD_SETTIME:
						if (packet[0]==MESH_CMD_SETTIME_LEN)
						{
							utc = ((uint32_t)packet[MESH_PACKET_CMD+1]+((uint32_t)packet[MESH_PACKET_CMD+2]<<8)+((uint32_t)packet[MESH_PACKET_CMD+3]<<16)+((uint32_t)packet[MESH_PACKET_CMD+4]<<24));
							mesh_setunixtime (utc);
#ifdef TRACE
							sputs ("Mesh Set Time"); sput32(utc); sputnl();
#endif
						}
						else
						{
#ifdef TRACE
							sputs ("Mesh invalid set time\r\n");
#endif
						}
						break;
					case MESH_CMD_SET_FIRMWARE:
						if (packet[0]==DFU_SET_FIRMWARE_LEN)
							dfu_set_firmware (&packet[0]);
						break;
					default:
		#ifdef TRACE
						sputs ("invalid mesh command"); sput8 (packet[1]); sputnl();
		#endif
						break;
				}
				ul_seqr = packet[MESH_PACKET_SEQS];
			}
		}
		
//  Handle send data
		
		if (buffer_remove (&rx_packet[0], &uplinkqueue))
		{
			uint8_t i;
			for (i=0;i<packet[0];i++)
			{
				tx_packet[MESH_PACKET_CMD+i] = rx_packet[1+i];
			}
			tx_packet[0] = MESH_PACKET_CMD + rx_packet[0];
#ifdef TRACE
			sputs ("new packet\r\n");
#endif
		}
		else
		{
#ifdef TRACE
			sputs ("idle\r\n");
#endif
			tx_packet[MESH_PACKET_LENGTH] = MESH_PACKET_IDLE_LEN;
			tx_packet[MESH_PACKET_TYPE] = MESH_PACKET_TYPE_IDLE;
			tx_packet[MESH_PACKET_SEQS] = ul_seqs;
			tx_packet[MESH_PACKET_SEQR] = ul_seqr;
		}
		
//  Handle ack and send
	
		tx_packet[MESH_PACKET_SEQS] = ul_seqs;
		tx_packet[MESH_PACKET_SEQR] = ul_seqr;	
//		spacket (tx_packet);
		nrf_delay_us (SendDelayAfterSlot);
		send_packet (MESH_FREQ, &tx_packet[0]);
		radio_wait_send_idle();
		copylast ();
	}
}

uint8_t hub_already_present (uint8_t *deviceaddress)
{
uint8_t i;
	
	for (i=0;i<MAX_HUBS;i++)
	{
		if (hubs[i].connected)
		{
			if (deviceaddress_matches (&hubs[i].deviceaddress[0], &deviceaddress[0]))
				return i;
			break;
		}
	}
	return (nullhubindex);
}


void add_hub (uint8_t *deviceaddress, int8_t rssi)
{
uint16_t i;
#ifdef TRACE
			sputs ("add_hub during page"); 
#endif	
	i = hub_already_present (deviceaddress);
	if (i != nullhubindex)
	{
		hubs[i].connected = 1;
		hubs[i].dl_seqs = 0;
		hubs[i].dl_seqr = null_seq;
		hubs[1].tx_ackpending = 0;
		hubs[i].rx_ackpending = 0;
		hubs[i].supervision_timeout_count = 0;
		
		return;
	}
	
	for (i=0;i<MAX_HUBS;i++)
	{
		if (hubs[i].connected==0)
		{
			hubs[i].connected = 1;
			hubs[i].dl_seqs = 0;
			hubs[i].dl_seqr = null_seq;
			hubs[1].tx_ackpending = 0;
			hubs[i].rx_ackpending = 0;
			hubs[i].settime = 1;
			deviceaddress_copy (&hubs[i].deviceaddress[0], &deviceaddress[0]);
			hubs[i].supervision_timeout_count = 0;
			hubcount++;
			copydest (i);
			tx_packet[MESH_PACKET_RSSI] = rssi;
			setcmd (MESH_CONNECT, MESH_PAYLOAD_CONNECT_LEN);
			buffer_queue (&uplinkqueue, tx_packet);
#ifdef TRACE
			sputs ("add_hub"); sput8(hubcount); spacket(tx_packet);
#endif
			break;
		}
	}
}

void remove_hub (uint8_t ind)
{
	hubs[ind].connected = 0;
	hubcount--;
	copydest(ind);
	setcmd (MESH_DISCONNECT, MESH_PAYLOAD_DISCONNECT_LEN);
	buffer_queue (&uplinkqueue, tx_packet);
#ifdef TRACE
	sputs ("remove_hub"); sput8(ind); spacket(tx_packet);
#endif
}

void mesh_ul_receive_handler (uint8_t packet[], int8_t rssi)
{
	mesh_receive_flag = 1;
//	spacket(packet);
	if (packet[MESH_PACKET_LENGTH]<MESH_PACKET_CMD)
		return;
	if ((packet[MESH_PACKET_TYPE]!=MESH_PACKET_TYPE_PAYLOAD) && (packet[MESH_PACKET_TYPE]!=MESH_PACKET_TYPE_IDLE) )
		return;
	if (mesh_receive_page)
	{
		add_hub (&packet[MESH_PACKET_SELFID], rssi);
	}
	else
	{
#ifdef TRACE
		sputs(".");
//		sputs("UL  ack/seq:"); sput8(hubs[currenthub].tx_ackpending); sput8 (packet[MESH_PACKET_SEQR]); sputnl();
#endif
		hubs[currenthub].supervision_timeout_count = 0;
		
		if ( (hubs[currenthub].tx_ackpending) && (packet[MESH_PACKET_SEQR] == hubs[currenthub].dl_seqs))
		{
			hubs[currenthub].tx_ackpending = 0;
			if (hubs[currenthub].settime)
				hubs[currenthub].settime = 0;
			hubs[currenthub].dl_seqs++;
		}
		if (packet[MESH_PACKET_TYPE]==MESH_PACKET_TYPE_PAYLOAD)
			buffer_queue (&uplinkqueue, packet);
		hubs[currenthub].dl_seqr = packet[MESH_PACKET_SEQS];
	}
}

void handle_handshake (uint8_t page)
{

	if (page)
	{
#ifdef TRACE
//	sputs ("handle_handshake_page\r\n");
#endif
		tx_packet[MESH_PACKET_SEQS] = 0;
		tx_packet[MESH_PACKET_SEQR] = 0;
	}
	else
	{
#ifdef TRACE
//	sputs ("handle_handshake_hub"); sput8(currenthub); sput8 (hubs[currenthub].dl_seqs); sput8 (hubs[currenthub].dl_seqr);sputnl();
#endif
		tx_packet[MESH_PACKET_SEQS] = hubs[currenthub].dl_seqs;
		tx_packet[MESH_PACKET_SEQR] = hubs[currenthub].dl_seqr;
	}
	receive_stop();
	nrf_gpio_pin_set (MESH_SYNC_PIN);
	send_packet (MESH_FREQ, &tx_packet[0]);
//	spacket (tx_packet);
	radio_wait_send_idle();
	nrf_gpio_pin_clear (MESH_SYNC_PIN);	

	mesh_receive_flag = 0;
	mesh_receive_page = page;
	receive_start (MESH_FREQ, mesh_ul_receive_handler);	
	mesh_timeout_start (MESH_TIMEOUT_US);
	while (1)
	{
		if (mesh_timeout())
		{
			receive_stop();
			if (page==0)
			{
				hubs[currenthub].supervision_timeout_count++;
				if (hubs[currenthub].supervision_timeout_count > MESH_SUPERVISION_TIMEOUT_MAX)
				{
					//disconnect
					remove_hub(currenthub);
				}
			}
			break;
		}
		if (mesh_receive_flag)
		{
			receive_stop();
			break;
		}
	}
	mesh_timeout_stop();
}
	
void handle_page (void)
{
#ifdef TRACE
//	sputs ("handle_page"); sput8(hubcount); sputnl(); 
#endif
	zerodest();
	if (hubcount==MAX_HUBS)
		setpackettype( MESH_PACKET_TYPE_BROADCAST_CLOSED, MESH_PACKET_BROADCAST_LEN);
	else if (OUR_DEVICE_TYPE == DEVICE_TYPE_COMMHUB)
		setpackettype (MESH_PACKET_TYPE_BROADCAST_COMMHUB_OPEN, MESH_PACKET_BROADCAST_LEN);
	else
		setpackettype (MESH_PACKET_TYPE_BROADCAST_ROOMHUB_OPEN, MESH_PACKET_BROADCAST_LEN);
	handle_handshake(1);
}

void handle_hub (void)
{

#ifdef TRACE
//	sputs ("handle_hub"); sput8(currenthub); sputnl();
#endif
	copydest(currenthub);
	if (hubs[currenthub].settime)
	{
		setcmd (MESH_CMD_SETTIME, MESH_CMD_SETTIME_LEN);
		unixtime_copy (&tx_packet[MESH_PACKET_CMD+1], unixtime_return ());
		tx_packet[MESH_PACKET_CMD+5] = 0;
		hubs[currenthub].tx_ackpending = 1;
	}
	else 
	{
		setpackettype (MESH_PACKET_TYPE_IDLE, MESH_PACKET_IDLE_LEN);
	}
	handle_handshake(0);
}

void handle_slaves (void)
{

	if (currenthub < hubcount)
	{
		handle_hub ();
		while (currenthub < hubcount)
		{
			currenthub++;
			if (hubs[currenthub].connected)
				break;
		}
	}
	else
	{
		handle_page();
		currenthub = 0;
	}

}		

uint8_t is_best_rssi (uint8_t packet[])
{
	if (best_rssi == (-128))
		return 0;
	else
	{
	uint16_t i;
		for (i=0;i<deviceaddress_len;i++)
			if (packet[1+i] != best_rssi_addr[i])
				return (0);
		return (1);
	}
}

void 	handle_flash (void)
{
	if (flash_erase_requested)
	{
#ifdef TRACE
		sputs ("flash_erase_requested"); sput32((uint32_t)flash_page_requested); sputnl();
#endif
		flash_page_erase (flash_page_requested);
		flash_erase_requested = 0;
	}
	else if (flash_write_requested)
	{
#ifdef TRACE
		sputs ("flash_erase_requested"); sput32((uint32_t)flash_page_requested); sput32 ((uint32_t)flash_image_requested); sputnl();
#endif
		flash_page_write (flash_page_requested, flash_image_requested);
		flash_write_requested = 0;
	}
}

void mesh_search_handler (uint8_t packet[], int8_t rssi)
{
	nrf_gpio_pin_clear (MESH_SYNC_PIN);
	if ( (rssi > MIN_HUB_RSSI) &&
		( (packet[MESH_PACKET_TYPE] == MESH_PACKET_TYPE_BROADCAST_COMMHUB_OPEN) || (packet[MESH_PACKET_TYPE] == MESH_PACKET_TYPE_BROADCAST_ROOMHUB_OPEN) )
		&& empty_packet (packet))
	{
		if ( (rssi > best_rssi) || ( (packet[MESH_PACKET_TYPE]==MESH_PACKET_TYPE_BROADCAST_COMMHUB_OPEN) && (best_rssi_type==MESH_PACKET_TYPE_BROADCAST_ROOMHUB_OPEN) ) )
		{
			best_rssi = rssi;
			best_rssi_type = packet[MESH_PACKET_TYPE];
			deviceaddress_copy (&best_rssi_addr[0], &packet[1]);
		}
	}
}

void mesh_search2_handler (uint8_t packet[], int8_t rssi)
{

	if ( (rssi > MIN_HUB_RSSI) &&
		( (packet[MESH_PACKET_TYPE] == MESH_PACKET_TYPE_BROADCAST_COMMHUB_OPEN) || (packet[MESH_PACKET_TYPE] == MESH_PACKET_TYPE_BROADCAST_ROOMHUB_OPEN) )
		&& empty_packet (packet) && is_best_rssi(packet))
	{
		nrf_gpio_pin_clear (MESH_SYNC_PIN);
#ifdef TRACE
		sputs("!");
#endif
		receive_stop ();
		mesh_timer_disable();
		hub_state = hub_idle;
		nrf_delay_us(DelayAfterFind);
		cycles = 0;
		mesh_timer_enable();	
		ul_seqs = 0;
		ul_seqr = null_seq;
	}
}

void mesh_idle_handler (uint8_t packet[], int8_t rssi)
{
	
	// handle clock adjust
	
	if (NRF_TIMER1->EVENTS_COMPARE[2])
	{
		if (NRF_TIMER1->EVENTS_COMPARE[3])
		{
			NRF_TIMER0->CC[0] = late_timer_stop_value;
			sputs("+");
		}
		else
		{
			NRF_TIMER0->CC[0] = timer_stop_value;
			sputs("0");
		}
	}
	else
	{
			NRF_TIMER0->CC[0] = early_timer_stop_value;
			sputs("-");
	}

	//  process dl packet
	
	handle_dl_rx_packet (packet, rssi);
}

void TIMER0_IRQHandler (void) 
{
	NRF_TIMER0->EVENTS_COMPARE[0]=0;
	
#ifdef meshroot
	
//  Handle root hub of the mesh case	
	
	if ((cycles&1)==0)
	{
		nrf_delay_us (SendDelayAfterSlotMaster);	
		handle_slaves();
	}
		
#else
	
//  Handle intermediate hub case
	
	if ((cycles&1)==0)
	{

//  Handle intermediate hub talks to master case
		switch (hub_state)
		{
			case hub_null:
//				sputs ("n");
				best_rssi = (-128);
				best_rssi_type = MESH_PACKET_TYPE_BROADCAST_ROOMHUB_OPEN;
				nrf_gpio_pin_set (MESH_SYNC_PIN);
				receive_start (MESH_FREQ, mesh_search_handler);
				hub_state = hub_searching;
				in_sync = 0;
				supervision_timeouts = MESH_SUPERVISION_TIMEOUT_MAX/2;
				supervision_hits = 0;
				best_count = 0;
				break;
			case hub_searching:
				if (best_rssi != (-128)) 
				{
//					sputs ("s");
					receive_stop();
					nrf_gpio_pin_set (MESH_SYNC_PIN);
					receive_start (MESH_FREQ, mesh_search2_handler);
				}
				else if (best_count < BEST_COUNT_MAX)
				{
					best_count++;
//					sputs ("b");
				}
				else
				{
//					sputs ("x");
					receive_stop();
					hub_state = hub_null;
				}
				break;
			
			case hub_connected:

			case hub_idle:
				in_sync = 1;
				mesh_receive_flag = 0;
				receive_stop();
				nrf_gpio_pin_set (MESH_SYNC_PIN);
				receive_start (MESH_FREQ, mesh_idle_handler);	
				mesh_timeout_start (MESH_TIMEOUT_US_SLAVE);
				NRF_TIMER1->EVENTS_COMPARE[2] = 0;
				NRF_TIMER1->EVENTS_COMPARE[3] = 0;
				while (1)
				{
					if (mesh_timeout())
					{
						receive_stop();
						supervision_timeouts++;
						if (supervision_timeouts > MESH_SUPERVISION_TIMEOUT_MAX)
							hub_state = hub_null;
						break;
					}
					if (mesh_receive_flag)
					{
						receive_stop();
						break;
					}
				}
				nrf_gpio_pin_clear (MESH_SYNC_PIN);
				mesh_timeout_stop();
				break;			
			default:
				hub_state = hub_null;
				break;
		}
	}
#ifdef meshedge
	else
	{
// Handle intermediate hub talks to slave case

		if (hub_state==hub_idle)
			handle_slaves ();
	}
#endif
#endif
	
//  Update cycle and LED status of mesh
	
	cycles++;
	if ( (cycles&1) || (in_sync) )
		led_on();
	else
		led_off();
 // nrf_gpio_pin_clear (MESH_SYNC_PIN);
	
	if ( (hub_state == hub_connected) )
	{
#ifdef TRACE
//		sputs ("badge listen\r\n");
#endif
#ifndef badge
#ifndef sniffer
		receive_start (BADGE_FREQ, mesh_badge_handler);	
#endif
#endif
		
	}

	handle_flash();

}



/*************************************************************************
  Definition of external functions
 *************************************************************************/

void mesh_write_flash (uint16_t pagenumber, uint32_t *page_image)
{
#ifdef TRACE
	sputs ("mesh_write_flash"); sput32(pagenumber*PAGE_SIZE); sput32((uint32_t)page_image); sputnl();
#endif
	flash_page_requested = (uint32_t *) (pagenumber*PAGE_SIZE);
	flash_image_requested = page_image;
	flash_erase_requested = 1;
	flash_write_requested = 1;
}

void mesh_setunixtime (uint32_t utc)
{
uint16_t i;
	
	unixtime_set (utc);
	for (i=0;i<hubcount;i++)
		hubs[i].settime = 1;
}

void mesh_start (void)
{
uint8_t i;
	
//  Initialize state machine

	cycles = 0;
	
#ifdef meshroot
	in_sync = 1;
	hub_state = hub_connected;
#else
	in_sync = 0;
	hub_state = hub_null;
#endif
	
	for (i=0;i<MAX_HUBS;i++)
	{
		hubs[i].connected = 0;
	}	

	mesh_timer_enable();
}

void mesh_end (void)
{
	mesh_timer_disable();
}

void mesh_send_uplink (uint8_t tx_packet[])
{
	tx_packet [MESH_PACKET_TYPE] = MESH_PACKET_TYPE_PAYLOAD;
	return_deviceaddress (&tx_packet[MESH_PACKET_SELFID]);
	buffer_queue (&uplinkqueue, tx_packet);
}

uint8_t mesh_return_event (uint8_t packet[])
{
	return (buffer_remove (packet, &uplinkqueue));
}

void mesh_init (void)
{

#ifdef TRACE
	sputs ("mesh_init\r\n");
#endif
	
	return_deviceaddress (&tx_packet[MESH_PACKET_SELFID]);	
	buffer_queue_init (&uplinkqueue);

//  comment 

	 nrf_gpio_cfg_output (MESH_SYNC_PIN);
   nrf_gpio_pin_clear (MESH_SYNC_PIN);
	
//  Initialize timer scheduler
	
	mesh_timer_start();

#ifdef NORPI
	 mesh_start();
#endif
}
