//  Buffers.c

/*************************************************************************
  SWIPESENSE Buffers
 *************************************************************************/

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "radio_config.h"
#include "nrf_gpio.h"
#include "app_timer.h"
#include "swipesense.h"
#include "serial.h"
#include "trace.h"
#include "badge.h"
#include "sniffer.h"
#include "mesh.h"
#include "buffers.h"

#include "nordic_common.h"
#include "nrf_error.h"
#include "unixtime.h"


/*************************************************************************
  DEFINES used here
 *************************************************************************/

#define buffer_block_len 19
#define buffer_blocks_max 1000
#define NIL 0xFFFF

/*************************************************************************
  Definition of state variables
 *************************************************************************/

struct {
	uint16_t nextr; 
	uint16_t nextd;
	uint8_t len;
	uint8_t data[buffer_block_len];
} buffer_block [buffer_blocks_max];

uint16_t free_bb;

/*************************************************************************
  Definition of internal functions
 *************************************************************************/

void free_blocks (uint16_t oldfront)
{
uint16_t oldfree;
	
	while(oldfront != NIL)
	{
		oldfree = free_bb;
		free_bb = oldfront;
		buffer_block[oldfront].nextr = oldfree;
		oldfront = buffer_block[oldfront].nextd;
	}
}

void copy_packet (uint8_t packet[], uint16_t oldfront)
{
uint16_t pos, i, len;
	
	pos = 1;
	while (oldfront != NIL)
	{
		len = buffer_block[oldfront].len;
		for (i=0;i<len;i++)
			packet[pos++] = buffer_block[oldfront].data[i];
		oldfront = buffer_block[oldfront].nextd;
	}
	packet[0] = pos-1;

}

/*************************************************************************
  Definition of internal functions
 *************************************************************************/

void buffer_queue_init (buffer_queue_t *queue)
{
#ifdef TRACE
	sputs ("buffer_queue_init\r\n");
#endif
	queue->front = NIL;
	queue->back = NIL;
}

uint8_t buffer_queue (buffer_queue_t *queue, uint8_t packet[])
{
uint16_t pos, len, thislen, i, newblock, secondblock;
	
#ifdef TRACE
	sputs ("buffer_queue\r\n");
#endif
	pos = 1;
	len = packet[0];

	if (free_bb == NIL)
		return 0;
	newblock = free_bb;
	free_bb = buffer_block[free_bb].nextr;
	buffer_block[newblock].nextr = NIL;
	if (queue->front!=NIL)
	{
		buffer_block[queue->front].nextr = newblock;
	}
	queue->front = newblock;
	if (queue->back==NIL)
		queue->back = newblock;
	
	while (1)
	{
		if (len > buffer_block_len)
			thislen = buffer_block_len;
		else
			thislen = len;
		buffer_block[newblock].nextd = NIL;
		buffer_block[newblock].len = thislen;		
		for (i=0;i<thislen;i++)
			buffer_block[newblock].data[i] = packet[pos++];
		if (len <= thislen)
			break;
		
		if (free_bb==NIL)
			return 0;//??? busted case
		secondblock = free_bb;
		free_bb = buffer_block[free_bb].nextr;
		buffer_block[newblock].nextd = secondblock;
		buffer_block[secondblock].nextd = NIL;	

		newblock = secondblock;
		len = len-thislen;
	}	
	return 1;
}

uint8_t buffer_remove (uint8_t packet[], buffer_queue_t *queue)
{
uint16_t oldback;
#ifdef TRACE
//	sputs ("buffer_remove\r\n");
#endif
	
	if (queue->back == NIL)
		return 0;
	copy_packet (packet, queue->back);
	oldback = (queue->back);
	queue->back = buffer_block[queue->back].nextr;
	if (queue->back == NIL)
		queue->front = NIL;
	free_blocks (oldback);
	return 1;

}

#ifdef TRACE	
uint16_t buffer_free_count (void)
{
uint16_t cur, total;
	
	total = 0;
	cur = free_bb;
	while (cur != NIL)
	{
		cur = buffer_block[cur].nextr;
		total++;
	}
	return (total);
}

uint16_t buffer_qtrace (buffer_queue_t *queue)
{

uint16_t currententry, preventry, currentbuffer, i;
	
	currententry = queue->back;
	sputs ("queue trace\r\n");
	if (currententry == NIL)
	{
			if (queue->front == NIL)
				sputs ("empty queue\r\n");
			else
				sputs ("empty queue and back/front wrong\r\n");
	}
	else
	{
		preventry = currententry;
		while (currententry!=NIL)
		{
			sputs ("/");
			sput16 (currententry);
			sputs ("=");
			currentbuffer = currententry;
			while (currentbuffer!=NIL)
			{
				sput16 (currentbuffer);
				sputs ("["); sput8 (buffer_block[currentbuffer].len);
					for (i=0;i<buffer_block[currentbuffer].len;i++)
						sput8 (buffer_block[currentbuffer].data[i]);
					sputs ("]");
					currentbuffer = buffer_block[currentbuffer].nextd;
			}
			sputnl();
			preventry = currententry;
			currententry = buffer_block[currententry].nextr;
		}
		if (queue->front == preventry)
			sputs ("front ok");
		else
		{
			sputs ("front wrong"); sput16(queue->front); sput16 (preventry);
		}
	}
	sputnl();

	return 1;
}
#endif

void buffers_init(void)
{
uint16_t	i;
	
#ifdef TRACE	
	sputs ("buffers_init\r\n");
#endif
	
	for (i=0;i<buffer_blocks_max-1;i++)
	{
		buffer_block[i].nextd = NIL;
		buffer_block[i].nextr = i+1;
	}
	buffer_block[i].nextd = NIL;
	buffer_block[buffer_blocks_max-1].nextr = NIL;
	free_bb = 0;
}
		
		
	
	

