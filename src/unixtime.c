//  Unixtime.c

#include "stdint.h"
#include "unixtime.h"
#include "app_error.h"
#include "app_timer.h"
#include "nrf52.h"
#include "nrf52_bitfields.h"
#include "unixtime.h"

/*************************************************************************
  DEFINES used here
 *************************************************************************/

#define TIMER_1SEC 32768


/*************************************************************************
  Definition of state variables
 *************************************************************************/
uint32_t unixtime_timer;
APP_TIMER_DEF(unixtime_timer);
uint32_t unixtime_counter;

/*************************************************************************
  Definition of functions
 *************************************************************************/


void unixtime_counter_handler (void *p_context)
{
	unixtime_counter++;
}

void unixtime_init (void)
{
		APP_ERROR_CHECK (app_timer_create (&unixtime_timer, APP_TIMER_MODE_REPEATED, unixtime_counter_handler));
		APP_ERROR_CHECK (app_timer_start (unixtime_timer, TIMER_1SEC, 0));
		unixtime_counter = 0;

}

void unixtime_set (uint32_t utc)
{
	unixtime_counter = utc;
}

uint32_t unixtime_return (void)
{
	return (unixtime_counter);
}

void return_deviceaddress (uint8_t *deviceaddress)
{
	deviceaddress[0] = NRF_FICR->DEVICEADDR[0] & 0xff;
	deviceaddress[1] = (NRF_FICR->DEVICEADDR[0]>>8) & 0xff;
	deviceaddress[2] = (NRF_FICR->DEVICEADDR[0]>>16) & 0xff;
	deviceaddress[3] = (NRF_FICR->DEVICEADDR[0]>>24) & 0xff;
	deviceaddress[4] = NRF_FICR->DEVICEADDR[1] & 0xff;
	deviceaddress[5] = (NRF_FICR->DEVICEADDR[1]>>8) & 0xff;

}
