#ifndef SWIPESENSE_H
#define SWIPESENSE_H

/*************************************************************************
  SWIPESENSE include file
 *************************************************************************/
//  Build instructions

//  select switches for different nodes
//  Badge - badge and lowpower
//  Wall sensor - mesh, meshedge, and wallsensor
//  Wall sensor test - trace, mesh, meshroot, wallsensor
//  Room hub - mesh, roomhub
//  Comm hub - serialif, mesh, meshroot
//  Comm hub test - trace, tracebitbang and serialif or neither, mesh, meshroot, NORPI

// Build switches


//#define iff
#define NORPI

// Badge
#if 0
#define OUR_DEVICE_TYPE DEVICE_TYPE_BADGE
//#define serialif
#define TRACE
//#define sniffer
#define badge
//#define mesh
//#define meshedge
//#define buffertest
//#define serialtest
//#define serialif
//#define meshroot
//#define tracespi
//#define roomhub
//#define lowpower
//#define wallsensor
//#define watchdog
#endif

//  RoomHub
#if 0
#define OUR_DEVICE_TYPE DEVICE_TYPE_ROOMHUB
#define TRACE
//#define sniffer
//#define badge
#define mesh
#define meshedge
//#define buffertest
//#define serialtest

//#define serialif
//#define meshroot
//#define tracespi
#define roomhub
//#define lowpower
//#define wallsensor
//#define watchdog
#endif

// wall sensor
#if 0
#define OUR_DEVICE_TYPE DEVICE_TYPE_WALLSENSOR
//#define serialif
//#define TRACE
//#define sniffer
//#define badge
#define mesh
#define meshedge
//#define buffertest
//#define serialtest
//#define meshroot
//#define roomhub
#define lowpower
#define wallsensor
//#define watchdog
#endif

//CommHub
#if 0
#define OUR_DEVICE_TYPE DEVICE_TYPE_COMMHUB
#define TRACE
#define TRACETHRUSERIAL
//#define sniffer
//#define badge
#define mesh
//#define meshedge
//#define buffertest
//#define serialtest

#define serialif
#define meshroot
//#define tracespi

//#define roomhub

//#define lowpower
//#define wallsensor
//#define watchdog
#endif

//  sniffer
#if 1
#define OUR_DEVICE_TYPE 0
//#define serialif
#define TRACE

#define sniffer
//#define badge
//#define mesh
//#define meshedge
//#define buffertest
//#define serialtest

//#define meshroot
//#define tracespi
//#define roomhub

//#define lowpower
//#define wallsensor
//#define watchdog
#endif

//  trace translator
#if 0
#define OUR_DEVICE_TYPE 0
//#define serialif
#define TRACE
#define TRACESLAVE
#define tracespi
//#define sniffer
//#define badge
//#define mesh
//#define meshedge
//#define buffertest
//#define serialtest

//#define meshroot
//#define tracespi
//#define roomhub

//#define lowpower
//#define wallsensor
//#define watchdog
#endif


/*************************************************************************
  DEFINES used here
 *************************************************************************/

//  Interrupt Priorities

#define FW_REVISION_LOC (0x3fc)
#define CONFIG_PAGE_LOC (0xf000)
#define RADIO_IRQ_PRI _PRIO_APP_MID
#define TIMER0_IRQ_PRI _PRIO_SD_LOW
#define UART0_IRQ_PRI 2
#define UART_TIMER_IRQ_PRI _PRIO_SD_LOWEST


//  Define pins

#define RX_PIN_NUMBER 16
#define TX_PIN_NUMBER 18
#define UART_SENSE_PIN 19

//#define RX_PIN_NUMBER 1
//#define TX_PIN_NUMBER 0
#define RTS_PIN_NUMBER 17
#define CTS_PIN_NUMBER 15

#define TRACE_SEL_PIN_NUMBER 25
#define TRACE_CLK_PIN_NUMBER 24
#define TRACE_DATA_PIN_NUMBER 23

#define MESH_SYNC_PIN 22
#define BADGE_SYNC_PIN 26

#define SCL_PIN 13
#define SDA_PIN 11

#ifdef iff
#define LED_PIN_NUMBER 8
#else
#define LED_PIN_NUMBER 20
#endif

#define LIS2DH_ADDRESS 0x19

//  Define radio

#define TX_POWER RADIO_TXPOWER_TXPOWER_Pos4dBm

#define MESH_FREQ 78
#define BADGE_FREQ 81























#endif
