//  Deviceaddress.c

#include "stdint.h"

#include "app_error.h"
#include "app_timer.h"
#include "nrf52.h"
#include "nrf52_bitfields.h"
#include "trace.h"

#include "swipesense.h"
#include "deviceaddress.h"


/*************************************************************************
  Definition of external functions
 *************************************************************************/

void return_deviceaddress (uint8_t *deviceaddress)
{
	deviceaddress[0] = NRF_FICR->DEVICEADDR[0] & 0xff;
	deviceaddress[1] = (NRF_FICR->DEVICEADDR[0]>>8) & 0xff;
	deviceaddress[2] = (NRF_FICR->DEVICEADDR[0]>>16) & 0xff;
	deviceaddress[3] = (NRF_FICR->DEVICEADDR[0]>>24) & 0xff;
	deviceaddress[4] = NRF_FICR->DEVICEADDR[1] & 0xff;
	deviceaddress[5] = (NRF_FICR->DEVICEADDR[1]>>8) & 0xff;

}


uint8_t deviceaddress_matches (uint8_t *da1, uint8_t *da2)
{
uint16_t i;

		for (i=0;i<deviceaddress_len;i++)
			if (da1[i]!=da2[i])
				return 0;
		return 1;	
}

void	deviceaddress_copy (uint8_t *dest, uint8_t *source)
{
uint8_t i;
	
	for (i=0;i<deviceaddress_len;i++)
		dest[i] = source[i];
}

