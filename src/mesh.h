#ifndef MESH_H
#define MESH_H

//  Mesh.h


/*************************************************************************
  MESH module
 *************************************************************************/


/*************************************************************************
  DEFINES used here
 *************************************************************************/

#define MESH_PACKET_TYPE_PAYLOAD 1
#define MESH_PACKET_TYPE_IDLE 2
#define MESH_PACKET_TYPE_BROADCAST_COMMHUB_OPEN 3
#define MESH_PACKET_TYPE_BROADCAST_CLOSED 4
#define MESH_PACKET_TYPE_BROADCAST_BADGE 5
#define MESH_PACKET_TYPE_CONNECT 6
#define MESH_PACKET_TYPE_BROADCAST_ROOMHUB_OPEN 7

#define MESH_CONNECT 0x42
#define MESH_DISCONNECT 0x43

#define MESH_PACKET_LENGTH 0
#define MESH_PACKET_TYPE 1
#define MESH_PACKET_SELFID (MESH_PACKET_TYPE+1)
#define MESH_PACKET_DESTID (MESH_PACKET_SELFID+deviceaddress_len)
#define MESH_PACKET_SEQS  (MESH_PACKET_DESTID+deviceaddress_len)
#define MESH_PACKET_SEQR (MESH_PACKET_SEQS+1)
#define MESH_PACKET_CMD (MESH_PACKET_SEQR+1)
#define MESH_PACKET_EVT (MESH_PACKET_CMD)



#define MESH_PACKET_RSSI (MESH_PACKET_CMD+1)
#define MESH_PACKET_DEVICETYPE (MESH_PACKET_RSSI+1)

#define MESH_PACKET_BROADCAST_LEN (MESH_PACKET_DESTID+deviceaddress_len)
#define MESH_PAYLOAD_CONNECT_LEN (MESH_PACKET_DEVICETYPE+1)
#define MESH_PAYLOAD_DISCONNECT_LEN (MESH_PACKET_CMD+1)
#define MESH_PACKET_IDLE_LEN (MESH_PACKET_SEQR+1)

#define MESH_CMD_SETTIME 1
#define MESH_CMD_SETTIME_LEN (MESH_PACKET_CMD+5)
#define MESH_CMD_SET_FIRMWARE 4

#define MESH_EVT_HYGIENE_OPPO 0x40
#define MESH_EVT_HYGIENE 0x41


#define MIN_DEVICE_TYPE 1
#define MAX_DEVICE_TYPE 4
#define DEVICE_TYPE_COMMHUB 1
#define DEVICE_TYPE_ROOMHUB 2
#define DEVICE_TYPE_WALLSENSOR 3
#define DEVICE_TYPE_BADGE 4

#define PAGE_SIZE 4096
#define IMAGE_SIZE 64

#define BADGE_CONNECTABLE_OFFSET 13
#define BADGE_CONNECTABLE_MASK 0x80
#define BADGE_CURRENT_FW_REV 9
#define BADGE_NEW_FW_REV 14
#define BADGE_LAST_BLOCK_NUM 16
#define BADGE_BROADCAST_LEN 19

/*************************************************************************
  Definition of functions
 *************************************************************************/

void mesh_write_flash (uint16_t pagenumber, uint32_t *page_image);
void mesh_send_uplink (uint8_t tx_packet[]);
uint8_t mesh_return_event (uint8_t packet[]);
void mesh_setunixtime (uint32_t utc);
void mesh_start(void);
void mesh_end (void);
void mesh_init (void);

#endif
