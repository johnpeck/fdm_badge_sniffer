/* Copyright (c) 2014 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */

/** @file
*
* @defgroup nrf_dev_button_radio_tx_example_main main.c
* @{
* @ingroup nrf_dev_button_radio_tx_example
*
* @brief Radio Transceiver Example Application main file.
*
* This file contains the source code for a sample application using the NRF_RADIO peripheral.
*
*/
//  Above is included as Nordic code has been integrated into this file

/*************************************************************************
  SWIPESENSE main
 *************************************************************************/

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "radio_config.h"
#include "nrf_gpio.h"
#include "app_timer.h"
#include "swipesense.h"

#include "trace.h"

#include "sniffer.h"
#include "mesh.h"

#include "nrf_delay.h"

#include "nordic_common.h"
#include "nrf_error.h"






#include "deviceaddress.h"



/*************************************************************************
  DEFINES used here
 *************************************************************************/

#define APP_TIMER_PRESCALER      0                           /**< Value of the RTC1 PRESCALER register. */
#define APP_TIMER_OP_QUEUE_SIZE  5                           /**< Size of timer operation queues. */

/*************************************************************************
  Definition of state variables
 *************************************************************************/


/*************************************************************************
  Definition of internal functions
 *************************************************************************/

/**@brief Function for initialization oscillators.
 */
void clock_initialization()
{
#ifndef badge
    /* Start 16 MHz crystal oscillator */
    NRF_CLOCK->EVENTS_HFCLKSTARTED = 0;
    NRF_CLOCK->TASKS_HFCLKSTART    = 1;

    /* Wait for the external oscillator to start up */
    while (NRF_CLOCK->EVENTS_HFCLKSTARTED == 0)
    {
        // Do nothing.
    }
#endif
		
    /* Start low frequency crystal oscillator for app_timer(used by bsp)*/
    NRF_CLOCK->LFCLKSRC            = (CLOCK_LFCLKSRC_SRC_RC << CLOCK_LFCLKSRC_SRC_Pos);
    NRF_CLOCK->EVENTS_LFCLKSTARTED = 0;
    NRF_CLOCK->TASKS_LFCLKSTART    = 1;

    while (NRF_CLOCK->EVENTS_LFCLKSTARTED == 0)
    {
        // Do nothing.
    }
}


/*************************************************************************
  Definition of external functions
 *************************************************************************/

/**
 * @brief Function for application main entry.
 * @return 0. int return type required by ANSI/ISO standard.
 */
int main(void)
{
  uint8_t own[deviceaddress_len];
  uint16_t i;
	
  //  Setup clock and rtc based timers	

  nrf_delay_us (30000);
  clock_initialization();
  nrf_delay_us (30000);
  APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_OP_QUEUE_SIZE, NULL);
		
	
  //  Enable trace mechanism if configured
	
#ifdef serialif
  trace_init(0);
#else
  trace_init(1);	
#endif	
  sputs ("\r\nHello Portaterm! I'm a -*- fdm_hardware_id_sniffer -*- version -v- ");
  sputdec (*(uint16_t *)(FW_REVISION_LOC),0);
  sputs ("-v- deviceaddress =");
  return_deviceaddress (&own[0]);
  for (i=0;i< deviceaddress_len; i++)
    sput8(own[i]);
  sputnl();
  trace_process();   //  required to flush out the above before turing
		     //  uart off for production code
#ifndef TRACE
  trace_deinit();
#endif


  //  Initialize communications modules


  radio_configure();

  //  Initialize high level communications;

#ifdef badge
  badge_init();
  NRF_POWER->DCDCEN = 1;					//  turns on DC DC converter
#endif
#ifdef sniffer
  sniffer_init();
#endif



  //  Idle Loop
	
  while (true)
    {

#ifdef lowpower
      __WFI();
#endif
			

#ifdef sniffer
      sniffer_process();
#endif

#ifndef serialif
#ifdef TRACE
      trace_process();
#endif
#endif

#ifdef watchdog
      kick_wdt();
#endif

    }
}


