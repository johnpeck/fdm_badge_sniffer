project_name = fdm_sniffer

linker_script = lib/ldscripts/transmitter_gcc_nrf52.ld


# Choose toolchain path based on platform
this_os := "$(shell uname)"
ifeq ($(this_os),"Linux")
  # Platform is Linux
  GNU_INSTALL_ROOT := /usr
  GNU_VERSION := 4.9.3
  GNU_PREFIX := arm-none-eabi
else ifeq ($(this_os),"Darwin")
  GNU_INSTALL_ROOT := /usr
  GNU_VERSION := 4.9.3
  GNU_PREFIX := arm-none-eabi
endif


# ------------------------- Done with configuration ---------------------

help:
	@echo 'Makefile for $(project_name) on $(this_os)                            '
	@echo '                                                                      '
	@echo 'Usage:                                                                '
	@echo '   make finalize            Build the hex file                        '
	@echo '   make flash               Flash the device                          '
	@echo '   make clean               Clean up temporary files                  '
	@echo '                                                                      '



OUTPUT_FILENAME = $(project_name)

export OUTPUT_FILENAME
#MAKEFILE_NAME := $(CURDIR)/$(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST))
MAKEFILE_NAME := $(MAKEFILE_LIST)
MAKEFILE_DIR := $(dir $(MAKEFILE_NAME) ) 

TEMPLATE_PATH = lib

MK := mkdir
RM := rm -rf

#echo suspend
ifeq ("$(VERBOSE)","1")
NO_ECHO := 
else
NO_ECHO := @
endif

# Toolchain commands
CC              := '$(GNU_INSTALL_ROOT)/bin/$(GNU_PREFIX)-gcc'
AS              := '$(GNU_INSTALL_ROOT)/bin/$(GNU_PREFIX)-as'
AR              := '$(GNU_INSTALL_ROOT)/bin/$(GNU_PREFIX)-ar' -r
LD              := '$(GNU_INSTALL_ROOT)/bin/$(GNU_PREFIX)-ld'
NM              := '$(GNU_INSTALL_ROOT)/bin/$(GNU_PREFIX)-nm'
OBJDUMP         := '$(GNU_INSTALL_ROOT)/bin/$(GNU_PREFIX)-objdump'
OBJCOPY         := '$(GNU_INSTALL_ROOT)/bin/$(GNU_PREFIX)-objcopy'
SIZE            := '$(GNU_INSTALL_ROOT)/bin/$(GNU_PREFIX)-size'

#function for removing duplicates in a list
remduplicates = $(strip $(if $1,$(firstword $1) $(call remduplicates,$(filter-out $(firstword $1),$1))))

#source common to all targets
C_SOURCE_FILES += \
                  $(TEMPLATE_PATH)/components/toolchain/system_nrf52.c \
                  $(TEMPLATE_PATH)/components/libraries/button/app_button.c \
                  $(TEMPLATE_PATH)/components/libraries/util/app_error.c \
                  $(TEMPLATE_PATH)/components/libraries/util/app_error_weak.c \
                  $(TEMPLATE_PATH)/components/libraries/fifo/app_fifo.c \
                  $(TEMPLATE_PATH)/components/libraries/timer/app_timer.c \
                  $(TEMPLATE_PATH)/components/libraries/util/app_util_platform.c \
                  $(TEMPLATE_PATH)/components/libraries/util/nrf_assert.c \
                  $(TEMPLATE_PATH)/components/libraries/util/nrf_log.c \
                  $(TEMPLATE_PATH)/components/libraries/uart/retarget.c \
                  $(TEMPLATE_PATH)/components/libraries/uart/app_uart_fifo.c \
                  $(TEMPLATE_PATH)/components/drivers_nrf/delay/nrf_delay.c \
                  $(TEMPLATE_PATH)/components/drivers_nrf/common/nrf_drv_common.c \
                  $(TEMPLATE_PATH)/components/drivers_nrf/gpiote/nrf_drv_gpiote.c \
                  $(TEMPLATE_PATH)/examples/bsp/bsp.c \
                  $(TEMPLATE_PATH)/external/segger_rtt/RTT_Syscalls_GCC.c \
                  $(TEMPLATE_PATH)/external/segger_rtt/SEGGER_RTT.c \
                  $(TEMPLATE_PATH)/external/segger_rtt/SEGGER_RTT_printf.c \
                  src/main.c \
                  src/trace.c \
                  src/deviceaddress.c \
                  src/sniffer.c \
                  src/uart.c \
                  src/radio.c



#assembly files common to all targets
ASM_SOURCE_FILES  = lib/components/toolchain/gcc/gcc_startup_nrf52.s

#includes common to all targets
INC_PATHS  = -Isrc/config/transmitter_pca10040
INC_PATHS += -Isrc/config
INC_PATHS += -I$(TEMPLATE_PATH)/components/libraries/util
INC_PATHS += -I$(TEMPLATE_PATH)/components/libraries/timer
INC_PATHS += -I$(TEMPLATE_PATH)/components/drivers_nrf/uart
INC_PATHS += -I$(TEMPLATE_PATH)/components/drivers_nrf/common
INC_PATHS += -I$(TEMPLATE_PATH)/components/drivers_nrf/radio_config
INC_PATHS += -I$(TEMPLATE_PATH)/components/drivers_nrf/config
INC_PATHS += -I$(TEMPLATE_PATH)/components/drivers_nrf/gpiote
INC_PATHS += -I$(TEMPLATE_PATH)/components/libraries/fifo
INC_PATHS += -I$(TEMPLATE_PATH)/examples/bsp
INC_PATHS += -I$(TEMPLATE_PATH)/components/toolchain/gcc
INC_PATHS += -I$(TEMPLATE_PATH)/components/drivers_nrf/nrf_soc_nosd
INC_PATHS += -I$(TEMPLATE_PATH)/components/toolchain
INC_PATHS += -I$(TEMPLATE_PATH)/components/libraries/uart
INC_PATHS += -I$(TEMPLATE_PATH)/components/device
INC_PATHS += -I$(TEMPLATE_PATH)/external/segger_rtt
INC_PATHS += -I$(TEMPLATE_PATH)/components/drivers_nrf/delay
INC_PATHS += -I$(TEMPLATE_PATH)/components/toolchain/CMSIS/Include
INC_PATHS += -I$(TEMPLATE_PATH)/components/drivers_nrf/hal
INC_PATHS += -I$(TEMPLATE_PATH)/components/libraries/button
INC_PATHS += -Isrc


OBJECT_DIRECTORY = build
LISTING_DIRECTORY = $(OBJECT_DIRECTORY)
OUTPUT_BINARY_DIRECTORY = $(OBJECT_DIRECTORY)

# Sorting removes duplicates
BUILD_DIRECTORIES := $(sort $(OBJECT_DIRECTORY) $(OUTPUT_BINARY_DIRECTORY) $(LISTING_DIRECTORY) )

#flags common to all targets
CFLAGS  = -DNRF_LOG_USES_UART=1
CFLAGS += -DNRF52_PAN_12
CFLAGS += -DNRF52_PAN_15
CFLAGS += -DNRF52_PAN_58
CFLAGS += -DSWI_DISABLE0
CFLAGS += -DNRF52_PAN_20
CFLAGS += -DNRF52_PAN_54
CFLAGS += -DNRF52_PAN_31
CFLAGS += -DNRF52_PAN_30
CFLAGS += -DNRF52_PAN_51
CFLAGS += -DNRF52_PAN_36
CFLAGS += -DNRF52_PAN_53
CFLAGS += -DCONFIG_GPIO_AS_PINRESET
CFLAGS += -DNRF52_PAN_64
CFLAGS += -DNRF52_PAN_55
CFLAGS += -DNRF52_PAN_62
CFLAGS += -DNRF52_PAN_63
CFLAGS += -DBSP_UART_SUPPORT
CFLAGS += -DBOARD_PCA10040
CFLAGS += -DNRF52
CFLAGS += -mcpu=cortex-m4
CFLAGS += -mthumb -mabi=aapcs --std=gnu99
CFLAGS += -Wall -Werror -O3 -g3
CFLAGS += -mfloat-abi=hard -mfpu=fpv4-sp-d16
# keep every function in separate section. This will allow linker to dump unused functions
CFLAGS += -ffunction-sections -fdata-sections -fno-strict-aliasing
CFLAGS += -fno-builtin --short-enums 

# keep every function in separate section. This will allow linker to dump unused functions
LDFLAGS += -Xlinker -Map=$(LISTING_DIRECTORY)/$(OUTPUT_FILENAME).map
LDFLAGS += -mthumb -mabi=aapcs -L lib/components/toolchain/gcc -T$(linker_script)
LDFLAGS += -mcpu=cortex-m4
LDFLAGS += -mfloat-abi=hard -mfpu=fpv4-sp-d16
# let linker to dump unused sections
LDFLAGS += -Wl,--gc-sections
# use newlib in nano version
LDFLAGS += --specs=nano.specs -lc -lnosys

# Assembler flags
ASMFLAGS += -x assembler-with-cpp
ASMFLAGS += -DCONFIG_GPIO_AS_PINRESET
ASMFLAGS += -DBOARD_PCA10036
ASMFLAGS += -DNRF52
ASMFLAGS += -DBSP_DEFINES_ONLY
#default target - first one defined
default: clean nrf52832_xxaa

#building all targets
all: clean
	$(NO_ECHO)$(MAKE) -f $(MAKEFILE_NAME) -C $(MAKEFILE_DIR) -e cleanobj
	$(NO_ECHO)$(MAKE) -f $(MAKEFILE_NAME) -C $(MAKEFILE_DIR) -e nrf52832_xxaa 




C_SOURCE_FILE_NAMES = $(notdir $(C_SOURCE_FILES))
C_PATHS = $(call remduplicates, $(dir $(C_SOURCE_FILES) ) )
C_OBJECTS = $(addprefix $(OBJECT_DIRECTORY)/, $(C_SOURCE_FILE_NAMES:.c=.o) )

ASM_SOURCE_FILE_NAMES = $(notdir $(ASM_SOURCE_FILES))
ASM_PATHS = $(call remduplicates, $(dir $(ASM_SOURCE_FILES) ))
ASM_OBJECTS = $(addprefix $(OBJECT_DIRECTORY)/, $(ASM_SOURCE_FILE_NAMES:.s=.o) )

vpath %.c $(C_PATHS)
vpath %.s $(ASM_PATHS)

OBJECTS = $(C_OBJECTS) $(ASM_OBJECTS)



nrf52832_xxaa: $(BUILD_DIRECTORIES) $(OBJECTS)
	@echo Linking target: $(OUTPUT_FILENAME).out
	$(NO_ECHO)$(CC) $(LDFLAGS) $(OBJECTS) $(LIBS) -o $(OUTPUT_BINARY_DIRECTORY)/$(OUTPUT_FILENAME).out
	$(NO_ECHO)$(MAKE) -f $(MAKEFILE_NAME) -C $(MAKEFILE_DIR) -e finalize

## Create build directories
$(BUILD_DIRECTORIES):
	echo $(MAKEFILE_NAME)
	$(MK) $@

# Create objects from C SRC files
$(OBJECT_DIRECTORY)/%.o: %.c
	@echo Compiling file: $(notdir $<)
	$(NO_ECHO)$(CC) $(CFLAGS) $(INC_PATHS) -c -o $@ $<

# Assemble files
$(OBJECT_DIRECTORY)/%.o: %.s
	@echo Compiling file: $(notdir $<)
	$(NO_ECHO)$(CC) $(ASMFLAGS) $(INC_PATHS) -c -o $@ $<


# Link
$(OUTPUT_BINARY_DIRECTORY)/$(OUTPUT_FILENAME).out: $(BUILD_DIRECTORIES) $(OBJECTS)
	@echo Linking target: $(OUTPUT_FILENAME).out
	$(NO_ECHO)$(CC) $(LDFLAGS) $(OBJECTS) $(LIBS) -o $(OUTPUT_BINARY_DIRECTORY)/$(OUTPUT_FILENAME).out


## Create binary .bin file from the .out file
$(OUTPUT_BINARY_DIRECTORY)/$(OUTPUT_FILENAME).bin: $(OUTPUT_BINARY_DIRECTORY)/$(OUTPUT_FILENAME).out
	@echo Preparing: $(OUTPUT_FILENAME).bin
	$(NO_ECHO)$(OBJCOPY) -O binary $(OUTPUT_BINARY_DIRECTORY)/$(OUTPUT_FILENAME).out $(OUTPUT_BINARY_DIRECTORY)/$(OUTPUT_FILENAME).bin


## Create binary .hex file from the .out file
$(OUTPUT_BINARY_DIRECTORY)/$(OUTPUT_FILENAME).hex: $(OUTPUT_BINARY_DIRECTORY)/$(OUTPUT_FILENAME).out
	@echo Preparing: $(OUTPUT_FILENAME).hex
	$(NO_ECHO)$(OBJCOPY) -O ihex $(OUTPUT_BINARY_DIRECTORY)/$(OUTPUT_FILENAME).out $(OUTPUT_BINARY_DIRECTORY)/$(OUTPUT_FILENAME).hex

finalize: genbin genhex echosize

genbin: $(OUTPUT_BINARY_DIRECTORY)/$(OUTPUT_FILENAME).out
	@echo Preparing: $(OUTPUT_FILENAME).bin
	$(NO_ECHO)$(OBJCOPY) -O binary $(OUTPUT_BINARY_DIRECTORY)/$(OUTPUT_FILENAME).out $(OUTPUT_BINARY_DIRECTORY)/$(OUTPUT_FILENAME).bin

## Create binary .hex file from the .out file
genhex: 
	@echo Preparing: $(OUTPUT_FILENAME).hex
	$(NO_ECHO)$(OBJCOPY) -O ihex $(OUTPUT_BINARY_DIRECTORY)/$(OUTPUT_FILENAME).out $(OUTPUT_BINARY_DIRECTORY)/$(OUTPUT_FILENAME).hex

echosize:
	-@echo ''
	$(NO_ECHO)$(SIZE) $(OUTPUT_BINARY_DIRECTORY)/$(OUTPUT_FILENAME).out
	-@echo ''

clean:
	$(RM) $(BUILD_DIRECTORIES)

cleanobj:
	$(RM) $(BUILD_DIRECTORIES)/*.o



# Flash the device.  Note that nrfjprog can not cope if the hex file
# is specified relative to the working directory.  
flash: nrf52832_xxaa
	@echo Flashing: $(OUTPUT_BINARY_DIRECTORY)/$(OUTPUT_FILENAME).hex
	lib/nrfjprog/nrfjprog --erasepage 0x0-0x80000 -f nrf52
	cd $(OUTPUT_BINARY_DIRECTORY); ../lib/nrfjprog/nrfjprog --program $(OUTPUT_FILENAME).hex -f NRF52
	lib/nrfjprog/nrfjprog --reset -f NRF52

## Flash softdevice
